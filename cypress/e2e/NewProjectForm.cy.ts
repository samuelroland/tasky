// import NewProjectForm from "@/src/views/NewProjectForm.vue";

describe("NewProjectForm", () => {
  let store: any;

  beforeEach(() => {
    cy.visit("/new-project").then((window) => {
      store = window.__store__;
      store.createProject({
        name: "My great project",
        desc: "A very interesting project",
      });
    });
  });

  it("can create a new valid project", () => {
    expect(store.projects.length).to.equal(1);
    cy.getBy("project-name")
      .should("have.text", "Project name")
      .find("input")
      .should("have.value", "")
      .type("Super project!!");
    cy.getBy("project-desc")
      .should("have.text", "Project description")
      .find("input")
      .should("have.value", "")
      .type("What a description !");

    cy.getBy("project-submit")
      .should("include.text", "Create")
      .click()
      .then(() => {
        expect(store.projects.length).to.equal(2);
        expect(store.currentProject?.name).to.equal("Super project!!");
        expect(store.currentProject?.desc).to.equal("What a description !");
      });
    cy.getBy("project-desc").find("input").should("have.value", "");
    cy.getBy("project-desc").find("input").should("have.value", "");
    cy.contains("Successfully created a new project !");

    cy.getBy("back-home")
      .should("include.text", "Go back home")
      .should("be.visible")
      .click()
      .then(() => {
        cy.location("pathname").should("eq", "/tasks");
        cy.get("h1").contains("Super project!!"); //the current project is the new one
      });
  });
});
