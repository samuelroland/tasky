describe("welcome page", () => {
  beforeEach(() => {
    cy.visit("");
    cy.get("[data-test=welcome-buttons] a").as("buttons");
  });

  it("shows a welcome message and description", () => {
    cy.get("h1").contains("Welcome to Tasky!");
    cy.contains(
      "A minimalist but productive Todo-list app with offline support, fully tested, made in VueJS and other technologies to practice, learn and experiment."
    );

    cy.get("@buttons")
      .should("have.length", 2)
      .and("include.text", "Booot")
      .and("include.text", "More");
  });

  it("is possible to click on the buttons and get redirected", () => {
    cy.get("@buttons").eq(0).click();
    cy.location("pathname").should("eq", "/tasks");
    cy.visit("/");
    cy.get("@buttons").eq(1).click();
    cy.location("pathname").should("eq", "/about");
  });
});
