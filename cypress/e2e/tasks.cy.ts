import { should } from "chai";

describe("tasks page", () => {
  let store: any;
  beforeEach(() => {
    cy.visit("/tasks").then((window) => {
      store = window.__store__;
      cy.log(store);
      cy.log(store.tasks);
      store.createProject({
        name: "My great project",
        desc: "A very interesting project",
      });
    });
    cy.getBy("generate-tasks-btn").as("generate-btn");
    cy.getBy("tasks-list").as("tasks-list");
    cy.getBy("projects-list").as("projects-list");
  });

  it("displays a message when there are no project and tasks list should be hidden", () => {
    store.projects = [];
    expect(store.projects.length).to.equal(0);
    cy.contains("No project for the moment... please create one!");
    cy.get("body").should("not.include.text", "Tasks list");
    cy.get("button").contains("Create a new project").click();
    cy.location("pathname").should("eq", "/new-project");
  });

  it("shows the current project info and a create project button", () => {
    cy.get("h1").contains("My great project");
    cy.contains("A very interesting project");
    cy.get("button").contains("Create a new project").click();
  });

  it("shows all projects info", () => {
    store.createProject({
      name: "My other great project",
      desc: "A very very interesting project",
    });
    expect(store.projects.length).to.equal(2);
    store.projects.forEach((project: Project) => {
      cy.get("@projects-list").contains(project.name);
    });
  });

  it("can persist projects and tasks between refresh", () => {
    store.createProject({
      name: "A very important project",
      desc: "Make sure to not loose it...",
    });
    store.setCurrentProject(store.projects.length - 1);
    cy.get("h1").contains("A very important project");
    cy.contains("Make sure to not loose it...");
    cy.reload();
    cy.get("h1").contains("A very important project");
    cy.contains("Make sure to not loose it...");
  });

  it("can change the current project", () => {
    store.createProject({
      name: "Project 2",
      desc: "Desc of project 2",
    });
    store.createProject({
      name: "Project 3",
      desc: "Desc of project 3",
    });
    expect(store.projects.length).to.equal(3);
    store.projects.forEach((project: Project) => {
      cy.get("@projects-list").contains(project.name);
    });
    store.setCurrentProject(0);
    cy.getBy("project-0").should("have.class", "selected");
    cy.getBy("project-1")
      .should("not.have.class", "selected")
      .click()
      .should("have.class", "selected");
    cy.getBy("project-0").should("not.have.class", "selected");
  });

  it("should be able to generate fake tasks and mark a task as done", () => {
    cy.contains("You have 0 tasks and 0 done tasks in Tasky!");
    cy.get("@generate-btn").click();
    cy.contains("You have 3 tasks and 2 done tasks in Tasky!");
    cy.get("@tasks-list").children().should("have.length", 3);

    cy.get("@tasks-list").children().find("input[type=checkbox]").click();
    cy.contains("You have 3 tasks and 3 done tasks in Tasky!");
  });

  it("shows the task creation form", () => {
    cy.getBy("new-task-form").should("include.text", "New task");
  });

  it("can manage 2 projects with tasks", () => {
    cy.contains("You have 0 tasks and 0 done tasks in Tasky!");

    //Create a first task
    cy.getBy("tasks-list").find("div").should("have.length", 0);
    cy.getBy("task-name")
      .type("Read a book{enter}")
      .should("have.value", "")
      .then(() => {
        expect(store.tasks.length).to.equal(1);
        expect(store.currentTasks[0].name).to.equal("Read a book");
        expect(store.currentTasks[0].state).to.equal("todo");
        expect(store.currentTasks[0].priority).to.equal(10);
      });

    //Create a second task
    cy.getBy("tasks-list").children().should("have.length", 1);
    cy.contains("You have 1 tasks and 0 done tasks in Tasky!");
    cy.get("@tasks-list").contains("Read a book");
    cy.getBy("task-priority").clear().type("3").should("have.value", 3);
    cy.getBy("task-name")
      .type("Read a second book{enter}")
      .then(() => {
        expect(store.tasks.length).to.equal(2);
        expect(store.currentTasks[1].name).to.equal("Read a second book");
        expect(store.currentTasks[1].state).to.equal("todo");
        expect(store.currentTasks[1].priority).to.equal(3);
      });

    //Create a new project and switch to it
    cy.getBy("projects-list").find("button").click();
    cy.getBy("project-name").find("input").type("Podcast about lions");
    cy.getBy("project-desc").find("input").type("A great podcast about lions.");
    cy.getBy("project-submit").click();
    cy.getBy("back-home").click();
    cy.location("pathname").should("eq", "/tasks");

    //Create 4 new tasks in the project
    cy.getBy("tasks-list").should("not.contain", "Read a second book"); //tasks in other projects must not be visible
    cy.getBy("tasks-list").children().should("have.length", 0);
    cy.getBy("task-name")
      .type("Choose a title and description{enter}")
      .type("Create a first episode{enter}")
      .type("Selfhost a castopod instance{enter}")
      .type("Upload and publish the first episode :){enter}")
      .then(() => {
        expect(store.tasks.length).to.equal(6);
        expect(store.currentTasks.length).to.equal(4);
        expect(store.currentTasks[1].name).to.equal("Create a first episode");
        expect(store.currentTasks[1].state).to.equal("todo");
        expect(store.currentTasks[1].priority).to.equal(10);
      });
    cy.contains("You have 6 tasks and 0 done tasks in Tasky!");
  });
});
