# Tasky

**DISCLAIMER:** This is not meant for real use, just a for fun project.

![welcome page](models/Welcome.png)

I wanted to train myself on various new web technologies and I tought having a small project for experimentation would be fun... I would like to implement a nice, minimalist, fast, offline todolist app that is fully unit and e2e tested, that have a global store (Pinia) with the whole state managed in IndexedDB. I could even do performance tests, i.e. by generating thousands of tasks and mesuring interaction times.

This is going to help me with the ongoing development of [Delibay](https://delibay.org) as the frontend stores a lot of data in localStorage (I plan to migrate to IndexedDB), as I would like to use TypeScript (not learned yet), unit testing and cypress testing, and a bunch of other cool technologies.

## Features
- [ ] Create a task and store them in a Pinia store
- [ ] Make tasks storage persistent using IndexedDB
- [ ] Show tasks list
- [ ] Select and mark tasks as done (from keyboard)
- [ ] Add the possibility to create Projects to contains tasks
- [ ] Show a list of projects
- [ ] Enable projects edition
- [ ] Button to generate 10000 fake tasks to see how does it scale
- [ ] Create a rich task creation input with integrated commands ("buy some bread #administration p1" -> the task "buy some bread" in project Administration with a priority 1)

## Data system
Project:
- String name
- String description

Task:
- String name
- String description
- Task parent
- Number priority (1 to 4, 4 is the default, inspired by Todoist system)

## Tools and concepts to learn
1. Composition API in VueJS
1. Pinia ORM
1. TypeScript
1. Cypress
1. Vitest or Jest
1. IndexedDB
1. PWA
1. Fully offline SPA (files cached)
1. Service workers
1. Keyboard shorcut system
1. Local storage migration
1. Create and use a small design system with TailwindCSS
1. Websockets
1. Real-time collaborative work
1. Conflict resolution at sync
1. Sync queue

## License
This work is released under the [MIT](LICENSE) license. `Copyright (c) 2023-present Samuel Roland`

```
MIT License

Copyright (c) 2023-present Samuel Roland

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```

---
## Experience
Here are a few notes about the experience of using these tools, some working combination or notes for my future self.

TODO
