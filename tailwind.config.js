/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./cypress/support/component-index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        calm: "#efe3cd", //light orange
        calmer: "#EFEDE8", //extremly light orange
        force: "#e87808", //strong orange from the top of the T bar in the logo
      },
    },
  },
  plugins: [],
};
