import NewTaskForm from "./NewTaskForm.vue";

describe("NewTaskForm", () => {
  beforeEach(() => {
    const newTaskEvent = cy.spy().as("newTaskEvent");
    cy.mount(NewTaskForm, { props: { onNewTask: newTaskEvent } });
  });

  it("can be filled and cleaned", () => {
    cy.getBy("task-name").type("A new task{enter}").should("have.value", "");
  });

  it("emit an event to send the task if not empty", () => {
    //Expect to not emit event as the task name is empty
    cy.getBy("task-name").type("{enter}");
    cy.get("@newTaskEvent").should("not.have.been.called");

    //Expect to has emitted an event
    cy.getBy("task-name").type("A great new task{enter}");
    cy.get("@newTaskEvent")
      .should("have.callCount", 1)
      .should("have.been.calledWith", {
        name: "A great new task",
        state: "todo",
        priority: 10,
        id: 1,
      });
  });

  it("can change the priority of the task and has a default value of 10", () => {
    cy.getBy("task-priority").should("have.value", 10).clear().type(5);
    cy.getBy("task-name").type("A new task{enter}").should("have.value", "");
    cy.getBy("task-priority").should("have.value", 10);

    cy.get("@newTaskEvent")
      .should("have.callCount", 1)
      .should("have.been.calledWith", {
        name: "A new task",
        state: "todo",
        priority: 5,
        id: 1,
      });
  });

  it("can only set the priority of the task between 1 and 10", () => {
    //Value above 10
    cy.getBy("task-priority").should("have.value", 10).clear().type("15");
    cy.getBy("task-name").click(); //just focus another element to focusout the task
    cy.getBy("task-priority").should("have.value", 10);

    cy.getBy("task-name").type("A new task{enter}").should("have.value", "");
    cy.getBy("task-priority").should("have.value", 10);

    cy.get("@newTaskEvent")
      .should("have.callCount", 1)
      .should("have.been.calledWith", {
        name: "A new task",
        state: "todo",
        priority: 10,
      });

    //Value under 1
    cy.getBy("task-priority").should("have.value", 10).clear().type("0");
    cy.getBy("task-name").click(); //just focus another element to focusout the task
    cy.getBy("task-priority").should("have.value", 1);

    cy.getBy("task-name").type("A new task{enter}").should("have.value", "");
    cy.getBy("task-priority").should("have.value", 10);

    cy.get("@newTaskEvent")
      .should("have.callCount", 2)
      .should("have.been.calledWith", {
        name: "A new task",
        state: "todo",
        priority: 1,
      });
  });
});
