import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type { Task, Project } from "@/types";

export const useTasksStore = defineStore(
  "tasks",
  () => {
    //Projects
    const projects: Project[] = ref([]);
    const lastProjectId = ref(0);
    const currentProjectIndex = ref(0);

    const currentProject = computed((): Project | null => {
      console.log(currentProjectIndex.value);
      console.log(projects);
      console.log(currentProjectIndex.value < projects.value.length);
      if (
        currentProjectIndex.value != null &&
        currentProjectIndex.value < projects.value.length
      ) {
        return projects.value[currentProjectIndex.value];
      }
      return null;
    });

    function setCurrentProject(index: number) {
      currentProjectIndex.value = index;
    }

    function createProject(project: Project) {
      project.id = project.id != null ? project.id : lastProjectId.value++;
      projects.value.push(project);
    }

    //Tasks
    const tasks: Task[] = ref([]);
    const lastTaskId = ref(0);

    function init() {
      createTask({
        id: lastTaskId.value++,
        name: "buy butter",
        desc: "lorem ispum",
        priority: 3,
        state: "todo",
        project_id: currentProject.value.id,
      });
      createTask({
        id: lastTaskId.value++,
        name: "go to the gym",
        desc: "yeaaaahhh",
        priority: 7,
        state: "done",
        project_id: currentProject.value.id,
      });
      createTask({
        id: lastTaskId.value++,
        name: "Fix the cable",
        desc: "The one in the fridge",
        priority: 2,
        state: "archived",
        project_id: currentProject.value.id,
      });
    }

    function setAsDone(id: number): void {
      const foundTask = tasks.value.find((task: Task) => task.id == id);
      console.log(foundTask);
      if (foundTask != null) {
        foundTask.state = "done";
      }
    }

    function createTask(task: Task): void {
      task.project_id = currentProject.value.id;
      task.id = lastTaskId.value++;
      tasks.value.push(task);
    }

    const doneTasks = computed((): Task[] => {
      return tasks.value.filter((task) => task.state != "todo");
    });

    const currentTasks = computed((): Task[] => {
      return tasks.value.filter(
        (task) => task.project_id == currentProject.value?.id
      );
    });

    return {
      //Projects
      projects,
      createProject,
      setCurrentProject,
      currentProject,
      currentProjectIndex,
      lastProjectId,

      //Tasks
      tasks,
      currentTasks,
      init,
      setAsDone,
      createTask,
      doneTasks,
    };
  },
  {
    persist: true,
  }
);
