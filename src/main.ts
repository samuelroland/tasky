//Reusable logic for the app and Cypress component testing (in cypress/support/component.ts) has been moved to setup.ts to enable component testing in Cypress

import { createApp } from "./setup";

createApp().mount("#app");
