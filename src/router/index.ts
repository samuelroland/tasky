import { createRouter, createWebHistory } from "vue-router";
import TasksView from "@/views/TasksView.vue";
import HomeView from "../views/HomeView.vue";
import AboutView from "../views/AboutView.vue";
import NewProjectForm from "@/views/NewProjectForm.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/tasks",
      name: "tasks",
      component: TasksView,
    },
    {
      path: "/about",
      name: "about",
      component: AboutView,
    },
    {
      path: "/new-project",
      name: "new-project",
      component: NewProjectForm,
    },
  ],
});

export default router;
