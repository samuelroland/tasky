export interface Task {
  id?: number;
  name: string;
  desc?: string;
  project_id: number;
  priority: number;
  state: "todo" | "done" | "archived";
}

export interface Project {
  id?: number;
  name: string;
  desc: string;
}
