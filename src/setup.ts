import { createApp as CreateAppVue } from "vue";
import { createPinia } from "pinia";
import { useTasksStore } from "./stores/tasks";
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

export const createApp = () => {
  const pinia = createPinia();
  pinia.use(piniaPluginPersistedstate);
  const app = CreateAppVue(App).use(pinia).use(router);
  if (window.Cypress) {
    const tasksStore = useTasksStore();
    window.__store__ = tasksStore;
  }
  return app;
};
